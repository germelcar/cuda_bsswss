#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define N_LOOP 1000
#define TOTAL_C1 25
#define TOTAL_C2 47
#define N_SET_C1 15
#define N_SET_C2 27
#define START_C1 1
#define START_C2 (TOTAL_C1+1)

typedef enum { LEUKEMIA} DS_NAMES;
const int ROWS = 72;
const int COLS = 7130;
const int labels_length = N_SET_C1 + N_SET_C2;
const int aml_length = N_SET_C1;
const int all_length = N_SET_C2;
FILE* f;

float* read_ds_and_fill_array(DS_NAMES ds){

	float* array;
	char* ds_filename;
	int ds_rows;
	int ds_cols;
	FILE* fp;

	switch(ds){
		case LEUKEMIA:
			ds_filename = "db_leukemia.svm";
			ds_rows = 72;
			ds_cols = 7130;
			break;
	}

	array = (float*) malloc(sizeof(float) * ds_rows * ds_cols);

	if(array == NULL){
		printf("%s %s at line %d. %s.\n",
				"Error in", __FILE__, __LINE__,
				"No enough memory");
		return NULL;
	}

	if ((fp = fopen(ds_filename, "rt")) == NULL){
		printf("%s %s at line %d. %s %s.\n",
				"Error in", __FILE__, __LINE__,
				"Error reading file", ds_filename);
		return NULL;
	}

	unsigned long long index = 0;
	for(int i = 0; i < ds_rows; i++){
		fscanf(fp, "%f%*c", &array[index++]);
		for(int j = 1; j < ds_cols; j++)
			fscanf(fp, "%*d:%f%*c", &array[index++]);

	}

	fclose(fp);
	return array;
}

int* generate_integer_array(int rows, int cols){
	int* array = (int*) malloc(sizeof(int) * rows * cols);

	if(array == NULL){
		printf("%s %s at line %d. %s.\n",
				"Error in", __FILE__, __LINE__,
				"No enough memory");
		return NULL;
	}

	return array;
}

float* generate_float_array(int rows, int cols){
	float* array = (float*) malloc(sizeof(float) * rows * cols);

	if(array == NULL){
		printf("%s %s at line %d. %s.\n",
				"Error in", __FILE__, __LINE__,
				"No enough memory");
		return NULL;
	}

	return array;
}

void fill_from_min_to_max(int* array, int index,
		int min, int max, int start, int set, int end){

	index *= set;

	/* If min < max, then fill from min to max. Otherwise, fill
	 * from min to end, next from start to max.
	 */
	if(min < max){
		for(int i = min; i <= max; i++)
			array[index++] = i;
	}else{

		for(int i = min; i <= end; i++)
			array[index++] = i;

		for(int i = start; i <= max; i++)
			array[index++] = i;
	}

}

void do_round_cross(int* array, int start, int set, int end, int nloop){
	int min = start;
	int max = start + (set-1);

	for(int i = 0; i < nloop; i++){

		if(min == start && max == start + (set-1)){
			fill_from_min_to_max(array, i, min, max, start, set, end);
			min++;
			max++;

		}else{
			fill_from_min_to_max(array, i, min, max, start, set, end);

			if(max == end){
				max = start;
				min++;

			}else if(min == end){
				min = start;
				max++;
			}else{
				min++;
				max++;
			}
		}//End of else
	}//End of for-loop
}

__global__ void bsswss(float* data, int* aml, int* all,
		float* bsswss, int nloop){


	int tid = blockDim.x * blockIdx.x + threadIdx.x;

	/* if tid is equal to 0 or tid is greater equal to COLS, then, go out.
	 * Otherwise, do some taks.
	 */
	if(tid == 0 || tid >= COLS)
		return;


		float Xkj_aml			= 0.0f;
		float Xkj_all			= 0.0f;
		float Xj				= 0.0f;
		float Xij				= 0.0f;
		float total_Xkj_aml		= 0.0f;
		float total_Xkj_all		= 0.0f;
		float bss				= 0.0f;
		float wss				= 0.0f;
		int rowlabel			= 0;
		int rowdata				= 0;


		for(int i = 0; i < nloop; i++){

			//Calculating Xkj_aml, Xkj_all and Xj
			//Calculating Xkj_aml and Xj
			for(int j = 0; j < aml_length; j++){
				rowlabel = (i * aml_length) + j;
				rowlabel = aml[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xkj_aml += data[rowdata];
				Xj += data[rowdata];
			}

			//Calculating Xkj_all and Xj
			for(int j = 0; j < all_length; j++){
				rowlabel = (i * all_length) + j;
				rowlabel = all[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xkj_all += data[rowdata];
				Xj += data[rowdata];
			}


			//Dividing between arrays lengths.
			Xkj_aml = (Xkj_aml / aml_length);
			Xkj_all = (Xkj_all / all_length);
			Xj = (Xj / labels_length);


			//Calculating total for Xkj_aml, Xkj_all and bss
			total_Xkj_aml = ((Xkj_aml - Xj)*(Xkj_aml - Xj)) * aml_length;
			total_Xkj_all = ((Xkj_all - Xj)*(Xkj_all - Xj)) * all_length;
			bss = (total_Xkj_aml) + (total_Xkj_all);


			//Calculating Xij and wss
			for(int j = 0; j < aml_length; j++){
				rowlabel = (i * aml_length) + j;
				rowlabel = aml[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xij = data[rowdata];
				wss += ((Xij - Xkj_aml) * (Xij - Xkj_aml));
			}

			for(int j = 0; j < all_length; j++){
				rowlabel = (i * all_length) + j;
				rowlabel = all[rowlabel] - 1;
				rowdata = (rowlabel * COLS) + tid;
				Xij = data[rowdata];
				wss += ((Xij - Xkj_all)*(Xij - Xkj_all));
			}

			bsswss[(i * COLS) + tid] = bss/wss;
			bss = wss = Xj = Xij = total_Xkj_aml = total_Xkj_all = 0.0f;
			Xkj_aml = Xkj_all = 0.0f;

		}//End for-loop with i

}

int main(int argc, char** argv){

	//Host data
	float* array = read_ds_and_fill_array(LEUKEMIA);
	float* bsswss_array = generate_float_array(N_LOOP, COLS);
	int* aml = generate_integer_array(N_LOOP, N_SET_C1);
	int* all = generate_integer_array(N_LOOP, N_SET_C2);

	//Device data
	float* dev_array;
	float* dev_bsswss_array;
	int* dev_aml;
	int* dev_all;

	if(array == NULL || bsswss_array == NULL || aml == NULL || all == NULL){
		printf("%s %s at line %s. 1%s.\n",
				"Error in", __FILE__, __LINE__,
				"Error reserving memory");
		exit(EXIT_FAILURE);
	}

	do_round_cross(aml, START_C1, N_SET_C1, TOTAL_C1, N_LOOP);
	do_round_cross(all, START_C2, N_SET_C2, TOTAL_C1+TOTAL_C2, N_LOOP);

	//Reserving memory for device's pointers.
	cudaMalloc((void**)&dev_array, sizeof(float) * ROWS * COLS);
	cudaMalloc((void**)&dev_bsswss_array, sizeof(float) * N_LOOP * COLS);
	cudaMalloc((void**)&dev_aml, sizeof(int) * N_LOOP * N_SET_C1);
	cudaMalloc((void**)&dev_all, sizeof(int) * N_LOOP * N_SET_C2);


	//Copying data from host to device.
	cudaMemcpy(dev_array, array, sizeof(float) * ROWS * COLS,
			cudaMemcpyHostToDevice);

	cudaMemcpy(dev_aml, aml, sizeof(int) * N_LOOP * N_SET_C1,
			cudaMemcpyHostToDevice);

	cudaMemcpy(dev_all, all, sizeof(int) * N_LOOP * N_SET_C2,
			cudaMemcpyHostToDevice);

	cudaMemcpy(dev_bsswss_array, bsswss_array, sizeof(float) * N_LOOP * COLS,
			cudaMemcpyHostToDevice);


	//Launching kernel and starting time measure
	float time;
	long si, ui, sf, uf;
	struct timeval tv;

	gettimeofday(&tv,NULL);
	si = tv.tv_sec;
	ui = tv.tv_usec;

	//For GeForce GTX 670
	bsswss<<<7,1024>>>(dev_array, dev_aml, dev_all, dev_bsswss_array, N_LOOP);

	//Enable the line below for GeForce 420M or similars devices
	//bsswss<<<14,512>>>(dev_array, dev_aml, dev_all, dev_bsswss_array, N_LOOP);

	cudaDeviceSynchronize();

	gettimeofday(&tv,NULL);
	sf = tv.tv_sec;
	uf = tv.tv_usec;

	sf-=si;
	if(ui>uf){
		sf--;
		uf+=1000000;
	}
	uf-=ui;
	time = (float)sf;
	time += (float) uf/1000000;
	printf("Time: %f seconds\n", time);


	//Copying back the final bsswss array.
	cudaMemcpy(bsswss_array, dev_bsswss_array, sizeof(float) * N_LOOP * COLS,
			cudaMemcpyDeviceToHost);

	/* For checking runtime errors */
	//cudaError_t err = cudaGetLastError();
	//const char* str_err = cudaGetErrorString(err);

	f = fopen("results", "wt");

	unsigned long long index = 1;
	for(int i = 0; i < N_LOOP; i++){
		for(int j = 1; j < COLS; j++){
			fprintf(f, "%f\t", bsswss_array[index++]);
		}
		fprintf(f, "\n");
		index++;
	}

	fclose(f);

	//Freeing device's memory
	cudaFree(dev_array);
	cudaFree(dev_bsswss_array);
	cudaFree(dev_aml);
	cudaFree(dev_all);

	//Freeing host's memory
	free(array);
	free(bsswss_array);
	free(aml);
	free(all);

	return EXIT_SUCCESS;
}
